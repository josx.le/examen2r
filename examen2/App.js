import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

const URL = 'https://jsonplaceholder.typicode.com/todos';

const App = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const respuesta = await fetch(URL);
        const datos = await respuesta.json();
        setTodos(datos);
      } catch (error) {
        console.error("Error al obtener los datos:", error);
      }
    };

    fetchData();
  }, []);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Lista de Pendientes</Text>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes (solo IDs):</Text>
        <Text>{todos.map(pendiente => pendiente.id).join(", ")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes (IDs y Titles):</Text>
        <Text>{todos.map(pendiente => `ID: ${pendiente.id}, Título: ${pendiente.title}`).join("\n")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes sin resolver (ID y Title):</Text>
        <Text>{todos.filter(pendiente => !pendiente.completed).map(pendiente => `ID: ${pendiente.id}, Título: ${pendiente.title}`).join("\n")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes resueltos (ID y Title):</Text>
        <Text>{todos.filter(pendiente => pendiente.completed).map(pendiente => `ID: ${pendiente.id}, Título: ${pendiente.title}`).join("\n")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes (IDs y userID):</Text>
        <Text>{todos.map(pendiente => `ID: ${pendiente.id}, userID: ${pendiente.userId}`).join("\n")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes resueltos (ID y userID):</Text>
        <Text>{todos.filter(pendiente => pendiente.completed).map(pendiente => `ID: ${pendiente.id}, userID: ${pendiente.userId}`).join("\n")}</Text>
      </View>
      <View>
        <Text style={styles.subtitle}>Lista de todos los pendientes sin resolver (ID y userID):</Text>
        <Text>{todos.filter(pendiente => !pendiente.completed).map(pendiente => `ID: ${pendiente.id}, userID: ${pendiente.userId}`).join("\n")}</Text>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#557280',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default App;